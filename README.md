# GoodMine Minecraft Server
<div align="center">
![Шрифт.png](./Шрифт.png)

### IP: play.goodmine.net
### Интересные ссылки | Important Links
[Website](https://goodmine.net/) :: [Wiki](https://gitlab.com/goodmine-community/goodmine-common/-/wikis/home) :: [Discord](https://goodmine.net/discord) :: [VK](https://vk.com/goodmine) :: [YouTube](https://www.youtube.com/c/GoodMine)
</div>

## Идеи и предложения

Если ты хочешь **предложить что-то** или **написать свою идею** администрации или разработчикам, ты находишься в правильном месте..

Ты можешь [создать **таск**](https://gitlab.com/goodmine-community/goodmine-concept/-/issues/new) прямо сейчас и разработчики, а также персонал проекта сразу же увидят его!

## Техническая поддержка

Если ты хочешь создать **таск о баге** или **сообщение об ошибке** на GoodMine, то ты находишься *не совсем* в правильном месте!

Ты можешь перейти [**в нужное место**](https://gitlab.com/goodmine-community/goodmine-bug-report/-/issues) или [сразу **создать таск**](https://gitlab.com/goodmine-community/goodmine-bug-report/-/issues/new).

Обрати внимание, что для того, чтобы создать таск [необходимо бесплатно создать или войти в аккаунт GitLab](https://gitlab.com/users/sign_in?redirect_to_referer=yes).
